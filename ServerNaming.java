package fileSystem;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


public class ServerNaming 
{
	//Lista dei server directory presenti nel sistema
	private HashSet<Address> serverDir;
	private HashSet<Address> dataServer;
	private ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
	private Lock rLock = rwLock.readLock();
	private Lock wLock = rwLock.writeLock();
	private ReentrantReadWriteLock rwLock2 = new ReentrantReadWriteLock();
	private Lock rLock2 = rwLock2.readLock();
	private Lock wLock2 = rwLock2.writeLock();
	
	public ServerNaming()
	{
		serverDir = new HashSet<Address>();
		dataServer = new HashSet<Address>();
	}

	public static void main(String[] args)
	{
		ServerNaming sName = new ServerNaming();
		ServerSocket server = null;
		try
		{
			server = new ServerSocket(1234);
			System.out.println("ServerNaming active");
			while(true)
			{
				//chiamata per accettare le richieste
				Socket client = server.accept();
				Thread newConnection = new Thread(sName.new ServantName(client));
				newConnection.start();
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		try {
			server.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	class ServantName implements ServerNamingInterface, Runnable
	{	
		Socket client;
		ObjectInputStream ois = null;
		ObjectOutputStream oos = null;
		
		public ServantName(Socket s)
		{
			client = s;
		}
	
		public void run() 
		{
			try 
			{
				oos = new ObjectOutputStream(client.getOutputStream());
				ois = new ObjectInputStream(client.getInputStream());
				String command = (String)ois.readObject();
				System.out.println(Thread.currentThread().getName() + " active and running <" + command + ">");
				
				if(command.equals("clientRequest"))
				{
					Address server = clientRequest();
					oos.writeObject(server);
					oos.flush();
				}
				else if(command.equals("DataServer"))
				{
					Address pos = (Address)ois.readObject();
					HashSet<Address> ls = dataServerRequest(pos);
					oos.writeObject(ls);
					oos.flush();
					System.out.println(Thread.currentThread().getName() + " sending the list of DirectoryServers to the DataServer");
				}
				else if(command.equals("addServerDir"))
				{
					Address serverName = (Address)ois.readObject();
					boolean create = false;
					create = addServerDir(serverName);
					if(create) {
						rLock.lock();
						oos.writeObject(serverDir);
						rLock.unlock();
						rLock2.lock();
						oos.writeObject(dataServer);
						rLock2.unlock();
						wLock.lock();
						serverDir.add(serverName);
						wLock.unlock();
						ois.readObject();
					}
					else
						oos.writeObject(null);
					oos.flush();
				}
				else if(command.equals("checkActiveServerDir"))
				{
					Address serverName = (Address)ois.readObject();
					checkActiveServerDir(serverName);
				}
				else 
				{
					String error = "Bad Request";
					oos.writeObject(error);
					oos.flush();
				}
				System.out.println(Thread.currentThread().getName() + " task complete.");
				
				oos.close();
				ois.close();
				client.close();
			}
			catch(IOException | ClassNotFoundException ioe)
			{
				return;
			}
			
		}
	
		public Address clientRequest()
		{
			int max = serverDir.size();
			Address server = null;
			if(max > 0){
				server = this.chooseSD();
				System.out.println(Thread.currentThread().getName() + " sending: " + server.getIp() + " " + server.getPort() + " to the client");
			}
			return server;
		}
		private Address chooseSD() {
			Iterator<Address> tmp = serverDir.iterator();
			int tmpPos = ((int)(Math.random() * (serverDir.size()))); //per ora scelgo a caso su quale serverDir creare l'associazione
			int i = 0;			
			while(i!=tmpPos){
				tmp.next();
				i++;
			}
			Address pos = tmp.next();
			//serverDir.remove(pos); //NON HO CAPITO PERCHE' VIENE RIMOSSO
			return pos;
		}
		@Override
		public HashSet<Address> dataServerRequest(Address pos)
		{
			wLock2.lock();
			{
				//System.out.println("data");
				dataServer.add(pos);
			}
			wLock2.unlock();
			return serverDir;
		}
		public HashSet<Address> serverDirRequest()
		{
			return serverDir;
		}
		@Override
		public boolean addServerDir(Address name)
		{
			boolean exist = false;
			wLock.lock();
			{
				if(!serverDir.contains(name))
				{
					//serverDir.add(name);
					exist = true;
					System.out.println("Add Server Dir "+name);
				}
			}
			wLock.unlock();
			//TO-DO avvisare tutti i dataServer di dichiararsi al nuovo serverDir
			if(exist)
			{
				rLock.lock();
				{
					if(!serverDir.isEmpty())
					{
						Iterator<Address> iter = serverDir.iterator(); 
						while(iter.hasNext()) 
						{
							Address server = iter.next();
							try
							{
								Socket socket = new Socket(server.getIp(), server.getPort());
								ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
								ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
								
								String line = "subscribe";
								oos.writeObject(line);
								oos.writeObject(name);
								oos.flush();
								ois.readObject();
								oos.close();
								socket.close();
							}
							catch(IOException | ClassNotFoundException ioe)
							{
								System.out.println("Errore con server "+server);
								System.out.println(ioe);
							}
							
						}
					}
				}
				rLock.unlock();

				rLock2.lock();
				{
					if(!dataServer.isEmpty())
					{
						Iterator<Address> iter = dataServer.iterator(); 
						while(iter.hasNext()) 
						{
							Address server = iter.next();
							try
							{
								Socket socket = new Socket(server.getIp(), server.getPort());
								ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
								ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
								
								String line = "subscribe";
								oos.writeObject(line);
								oos.writeObject(name);
								oos.flush();
								ois.readObject();
								System.out.println("qui qui");
								oos.close();
								socket.close();
							}
							catch(IOException | ClassNotFoundException ioe)
							{
								System.out.println("Errore con server "+server);
								System.out.println(ioe);
							}
							
						}
					}
				}
				rLock2.unlock();
			}
			//System.out.println("naming-dir");		
			return exist;
		}
		@Override
		public void checkActiveServerDir(Address serverDirName)
		{
			Address server = null;
			wLock.lock();
			{
				Address pos = null;
				Iterator<Address> iter = serverDir.iterator();
				while(iter.hasNext())
				{
					Address sDName = iter.next();
					if(serverDirName.equals(sDName))
						pos = sDName;
				}
				if(pos != null)
				{
					server = pos;
					serverDir.remove(pos);
				}
				else {
					wLock.unlock();
					return;
				}
					
			}
			wLock.unlock();
			
			Socket s = null;
			try
			{
				String func = new String("status");
				boolean status = false;
				s = new Socket(server.getIp(), server.getPort());
				
				ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
				ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
			
				oos.writeObject(func);
				oos.flush();
				status = (boolean)ois.readObject();
				if(status)
					addServerDir(server);
				else
					System.out.println("Directory Server "+ serverDirName+" down, check server manually");
			}
			catch(Exception e)
			{
				System.out.println("ERROR : "+e);
				System.out.println("Directory Server "+ serverDirName+" down, check server manually! ");
			}
			try{
				s.close();
			}
			catch(Exception e)
			{
				System.out.println("ERROR : "+e);
			}

		}
		
	}

}
