package fileSystem;
import java.util.*;
import java.io.*;

public class DirectoryTree
{
	private class DirectoryNode
	{
		public String name;
		public List<DirectoryNode> children;
		public int nchildren;
		public DirectoryNode parent;
		
		// costructor
		public DirectoryNode()	
		{
			name = null;
			children = new ArrayList<DirectoryNode>();
			parent = null;
			nchildren = 0;
		}
		public DirectoryNode(String s)	
		{
			name = s;
			children = new ArrayList<DirectoryNode>();
			nchildren = 0;
		}
		public DirectoryNode(String s, DirectoryNode p)	
		{
			name = s;
			parent = p;
			nchildren = 0;
			children = new ArrayList<DirectoryNode>();
		}
		public String toString()
		{
			return name+"with "+nchildren+" children";
		}
	}

	public DirectoryNode root;
	public DirectoryTree()
	{
		root = new DirectoryNode("root");
	}
	public int contains(String s)	// 0 nothing 1 parent folder exist 2 folder exist
	{
		Scanner scan = new Scanner(s).useDelimiter("/");
		DirectoryNode n = root;
		int res = 0;
		int find = 0;
		String child = null;
		while(scan.hasNext())
		{
			find++;
			child = scan.next();
			for(int i = 0; i < n.nchildren; i = i + 1)
			{
				if(child.compareTo(n.children.get(i).name) == 0)
				{
					n = n.children.get(i);
					res++;
					break;
				}
			}
		}
		scan.close();
		if(find == res)
			return 2;
		else if(find -1 == res )
			return 1;
		return 0;
	}
	public List<String> getDirectory(String s)
	{
		DirectoryNode n = findNode(s);
		if(n == null)
			return null;
		return childrenList(n);
	}
	public boolean addDirectory(String dir)
	{
		Scanner scan = new Scanner(dir).useDelimiter("/");
		DirectoryNode n = root;
		int res = 0;
		int find = 0;
		int add = 0;
		String child = null;

		while(scan.hasNext() && add == 0)
		{
			find++;
			child = scan.next();
			for(int i = 0; i < n.nchildren; i = i + 1)
			{
				if(child.compareTo(n.children.get(i).name) == 0)
				{
					n = n.children.get(i);
					res++;
					break;
				}
			}
			if(find > res)
			{
				add = 1;
			}
		}
		if(add == 1)
		{
			DirectoryNode nadd = new DirectoryNode(child,n);
			//System.out.println("Nodo : "+n.name+" figli: "+n.nchildren+" voglio aggiungere: "+child);
			addChildren(n , nadd);
			n = nadd;
			while(scan.hasNext())
			{
				child = scan.next();
				if(child == null)
					break;
				//System.out.println("Nodo : "+n.name+" figli: "+n.nchildren+" voglio aggiungere: "+child);
				nadd = new DirectoryNode(child,n);
				addChildren(n , nadd);
				n = nadd;
			}
			scan.close();
			return true;
		}
		scan.close();
		return false;
	}
	public boolean renameDirectory(String parent, String s)
	{
		DirectoryNode n = findNode(parent);
		if(n == null)
			return false;
		n.name = s;
		return true;
	}
	public boolean removeDirectory(String dir)
	{
		DirectoryNode child = findNode(dir);
		if(child == null)
			return false;
		return removeChild(child.parent, child.name);
	}


	public ArrayList<String> findAllchild(String parent)
	{
		ArrayList<String> childs = new ArrayList<>();
		DirectoryNode n = findNode(parent);
		
		ArrayList<DirectoryNode> childNode = new ArrayList<DirectoryNode>();
		
		if(n != null)
			childNode.addAll(n.children);
		

		for(int i = 0; i < childNode.size(); i++)
		{
			DirectoryNode newChild = childNode.get(i);
			childs.add(parent+ "/" + newChild.name);
			if(newChild.children != null)
				childs.addAll(findAllchild(parent+"/"+childNode.get(i).name));
				
		}
		return childs;
	}
	
	public DirectoryNode findNode(String s)
	{
		if(contains(s) != 2)
			return null;
		Scanner scan = new Scanner(s).useDelimiter("/");
		DirectoryNode n = root;
		String child = null;
		while(scan.hasNext())
		{
			child = scan.next();
			for(int i = 0; i < n.nchildren; i = i + 1)
			{
				if(child.compareTo(n.children.get(i).name) == 0)
				{
					n = n.children.get(i);
					break;
				}
			}
		}
		scan.close();
		return n;
	}
	private List<String> childrenList(DirectoryNode n)
	{
		List<String> child = new ArrayList<String>();
		for(int i = 0; i < n.nchildren; i = i + 1)
		{
			child.add(n.children.get(i).name);
		}
		return child;
	}
	private boolean addChildren(DirectoryNode parent, DirectoryNode child)
	{
		parent.children.add(child);
		parent.nchildren = parent.nchildren + 1;
		//System.out.println("Done! addChildren "+ child.name+" to "+ parent.name);
		return true;
	}
	private boolean removeChild(DirectoryNode parent, String s)
	{
		for(int i = 0; i < parent.nchildren; i = i + 1)
		{
			if(s.compareTo(parent.children.get(i).name) == 0)
			{
				parent.children.remove(i);
				parent.nchildren = parent.nchildren - 1 ;
				return true;
			}
		}
		return false;
	}
}