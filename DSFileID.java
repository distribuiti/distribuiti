package fileSystem;
import java.io.*;
public class DSFileID implements Serializable{

	private Address pos;
	private int key;
	private Address posCopy;
	private int keyCopy;
	
	public DSFileID(Address p, int k, Address copyP, int copyK)
	{
		pos = p;
		key = k;
		posCopy = copyP;
		keyCopy = copyK;
	}
	public DSFileID(Address p, int k)
	{
		pos = p;
		key = k;
	}
	
	public Address getPos()
	{
		return pos;
	}
	
	public int getKey()
	{
		return key;
	}
	
	public void setPosCopy(Address p)
	{
		posCopy = p;
	}
	
	public void setKeyCopy(int k)
	{
		keyCopy = k;
	}
	
	public Address getPosCopy()
	{
		return posCopy;
	}
	
	public int getKeyCopy()
	{
		return keyCopy;
	}
}
