package fileSystem;

import java.util.HashSet;

public interface ServerNamingInterface
{
	public Address clientRequest();
	public HashSet<Address> dataServerRequest(Address pos);
	public HashSet<Address> serverDirRequest();
	public boolean addServerDir(Address name);
	public void checkActiveServerDir(Address serverDirName);
}
