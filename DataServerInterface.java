package fileSystem;

public interface DataServerInterface
{
	public Integer createFile(String dir, String name, byte[] buffer, Address SDpos);
	public boolean deleteFile(Integer fileID);
	public byte[] readFile(Integer fileID);
	public boolean uploadFile(Integer fileID, byte[] buffer);
}
