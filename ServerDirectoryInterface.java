package fileSystem;

import java.util.Set;

public interface ServerDirectoryInterface
{
	public boolean createDir(String dir, String name);//verifica l'esistenza e in caso aggiunge la nuova directory nella struttura ad albero
	public boolean renameDir(String dir, String name);//verifica la presenza di conflitti e rinomina la directory sia sull'albero che sui serverdirectory
	public boolean deleteDir(String dir, String name);//si fa ritornare la lista delle directory da eliminare su ogni serverdirectory e controlla che non vi siano file in creazione o in upload su tali directory
	public Set<String> lookDirFiles(String dir);//ritorna la lista di tutti i file e le cartelle contenuti in dir

	public boolean fileIsHere(String dir, String name);//controlla la presenza del file
	
	public int isCreatableFile(String dir, String name);//casi: 1)il file non esiste e la directory non esiste; 2)il file non esiste e la cartella esiste;3)ci sono conflitti in scrittura;4)la directory sta per essere eliminata;5)tutto ok
	public boolean createFile(String dir, String name, byte[] buffer);//chiama isCreatable e in caso richiede la creazione del file ad un data server
	
	public DSFileID getFilePos(String dir, String name);//verifica che il file esista e in caso si fa ritornare la posizione del dataserver in cui e' salvato
	public byte[] getFile(String dir, String name);//richiede la lettura di un file al dataserver in cui e' fisicamente salvato
		
	public boolean uploadFile(String dir, String name, byte[] buffer);//esegue un aggiornamento del file specificato contenuto nel dataserver (non deve entrare in conflitto con altri upload dello stesso file)  
	public boolean deleteFile(String dir, String name);//elimina il file specificato contenuto nel dataserver e ne elimina qualsiasi riferimento (non deve entrare in conflitto con upload del file)
	
	public boolean createAssociation(String dir, String name, DSFileID file);//crea il riferimento al file specificato per tenerne traccia
	
	public boolean activation();//si attiva e lo comunica al servernaming
	public boolean status();//ritorna lo stao del server
}
