package fileSystem;
import java.io.*;
public class Address implements Serializable{

	private String ip;
	private Integer port;
	
	public Address(String ip, int port)
	{
		this.ip = new String(ip);
		this.port = new Integer(port);
	}
	
	public String getIp()
	{
		return ip;
	}
	
	public Integer getPort()
	{
		return port;
	}
	
	public boolean equals(Address addr2)
	{
		if(this.ip.equals(addr2.getIp()) && this.port == addr2.getPort())
			return true;
		else
			return false;
	}
	public int compareTo(Address addr2)
	{
		if(this.equals(addr2))
			return 0;
		if(this.port > addr2.getPort())
			return 1;
		return -1;
	}
	public String toString()
	{
		return this.ip+":"+this.port;
	}
}
