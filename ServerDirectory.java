package fileSystem;

import java.time.*;
import java.util.*;
import java.util.concurrent.locks.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;

public class ServerDirectory
{
		//lista dei server directory
		private HashSet<Address> serverDirList;
		private HashSet<Address> serverDataList;
		
		//tabella dei file e albero delle directory
		private HashMap<String, HashMap<String, DSFileID>> dirFilesTable;
		private DirectoryTree dirTree;
		
		//tabella dei file in scrittura, aggiornamento ed eliminazione

		private ArrayList<Map<String, ConcurrentDirFiles>> wudFiles;
		
		//lista delle cartelle in creazione, rinomina ed eliminazione
		private ArrayList<Map<String, ConcurrentDirFiles>> wrdDir;
		
		ReadWriteLock rwLock = new ReentrantReadWriteLock();
		Lock rl = null;
		Lock wl = null;
		
		ReadWriteLock rwLockDS = new ReentrantReadWriteLock();
		Lock rlds = null;
		Lock wlds = null;
		
		ReadWriteLock rwLockSDir = new ReentrantReadWriteLock();
		Lock rlsd = null;
		Lock wlsd = null;
		
		ReadWriteLock rwLockDir = new ReentrantReadWriteLock();
		Lock rlDir = null;
		Lock wlDir = null;
		
		ReadWriteLock rwLockFiles = new ReentrantReadWriteLock();
		Lock rlFiles = null;
		Lock wlFiles = null;
		
		ReadWriteLock rwLockTree = new ReentrantReadWriteLock();
		Lock rlTree = null;
		Lock wlTree = null;

		
	public ServerDirectory()
	{
		serverDirList = new HashSet<>();
		serverDataList = new HashSet<>();
		dirFilesTable = new HashMap<>();
		dirTree = new DirectoryTree();
		wudFiles = new ArrayList<>();
		wudFiles.add(new HashMap<String,ConcurrentDirFiles>());//indice 0 scrittura
		wudFiles.add(new HashMap<String,ConcurrentDirFiles>());//indice 1 aggiornamento
		wudFiles.add(new HashMap<String,ConcurrentDirFiles>());//indice 2 eliminazione
		
		wrdDir = new ArrayList<>();
		wrdDir.add(new HashMap<String,ConcurrentDirFiles>());//indice 0 scrittura
		wrdDir.add(new HashMap<String,ConcurrentDirFiles>());//indice 1 renomina
		wrdDir.add(new HashMap<String,ConcurrentDirFiles>());//indice 2 eliminazione
		
		rl = rwLock.readLock();
		wl = rwLock.writeLock();

		rlds = rwLockDS.readLock();
		wlds = rwLockDS.writeLock();
		
		rlsd = rwLockSDir.readLock();
		wlsd = rwLockSDir.writeLock();
		
		rlTree = rwLockTree.readLock();
		wlTree = rwLockTree.writeLock();
		
		
		rlDir = rwLockDir.readLock();
		wlDir = rwLockDir.writeLock();
		
		rlFiles = rwLockFiles.readLock();
		wlFiles = rwLockFiles.writeLock();
		
	}
	
	public static void main(String[] args)
	{
		ServerDirectory sDir = new ServerDirectory();
		ServerSocket server = null;
		System.out.println("Server Directory Activation");
		Address ServerNamingAddr;
		Address myAddr;
		ServerNamingAddr = new Address("localhost", 1234);
		myAddr = new Address(args[0], Integer.valueOf(args[1]));
		if(sDir.activation(ServerNamingAddr, myAddr))
		{
			try
			{
				server = new ServerSocket(myAddr.getPort());
				
				// ciclo infinito, in attesa di connessioni
				while(true)
				{
					// chiamata bloccante, in attesa di una nuova connessione
					Socket client = server.accept();
					System.out.println("accept");
					// la nuova richiesta viene gestita da un thread indipendente, si ripete il ciclo
					Thread nuovaConnessione = new Thread(sDir.new ServantDir(client));
					nuovaConnessione.start();
				}
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			try {
				server.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public boolean activation(Address ServerNamingAddr, Address myAddr) 
	{
			
			Socket server = null;
			
			try
			{
				server = new Socket(ServerNamingAddr.getIp(), ServerNamingAddr.getPort());
				
				ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
				ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
			
				oos.writeObject("addServerDir");
				oos.writeObject(myAddr);
				oos.flush();
				//System.out.println("dir");
				HashSet<Address> temp = (HashSet<Address>) ois.readObject();
				HashSet<Address> tempDSL = (HashSet<Address>) ois.readObject();
				serverDirList.addAll(temp);
				serverDataList.addAll(tempDSL);
				
				oos.writeObject(true);
				
				server.close();
				System.out.println("Server Directory active");
				return true;
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		
			return false;
	}
	
	class ServantDir implements ServerDirectoryInterface, Runnable
	{	
		Socket client;
		ObjectInputStream ois = null;
		ObjectOutputStream oos = null;
		
		public ServantDir(Socket s)
		{
			client = s;
			
		}
	
		
		@Override
		public void run() {
			
			// TODO Auto-generated method stub
			try 
			{
				oos = new ObjectOutputStream(client.getOutputStream());
				ois = new ObjectInputStream(client.getInputStream());
				String command = (String)ois.readObject();
				System.out.println(Thread.currentThread().getName() + " active and running <" + command + ">");
				System.out.println("State of wrdDir \n"+wrdDir);
				System.out.println("State of wudDir \n"+wudFiles);
				System.out.println();
				if(command.equals("readFile"))
				{
					String dir = (String)ois.readObject();
					String file = (String)ois.readObject();

					byte[] buff = getFile(dir, file);
					oos.writeObject(buff);
					oos.flush();

				}
				if(command.equals("getMyFilePos"))
				{
					String dir = (String)ois.readObject();
					String file = (String)ois.readObject();

					DSFileID position = getMyFilePos(dir, file);
					
					oos.writeObject(position);
					oos.flush();
				}
				else if(command.equals("lookDirFiles"))
				{
					String dir = (String)ois.readObject();
					Set<String> ls = new HashSet<>();
					ls = lookDirFiles(dir);
					oos.writeObject(ls);
					oos.flush();
				}else if(command.equals("checkMyDir"))	// Method added 
				{
					String dir = (String)ois.readObject();
					Set<String> ls = new HashSet<String>();
					ls = checkMyDir(dir);
					oos.writeObject(ls);
					oos.flush();
				}
				else if(command.equals("createFile"))
				{
					String dir = (String)ois.readObject();
					String name = (String)ois.readObject();
					byte[] buffer = (byte[])ois.readObject();
					
					boolean success = createFile(dir, name, buffer);
					oos.writeObject(success);
					oos.flush();
				}
				else if(command.equals("checkCreateFile"))
				{
					String dir = (String)ois.readObject();
					String name = (String)ois.readObject();
					LocalDateTime time = (LocalDateTime)ois.readObject();
					
					int success = checkCreateFile(dir, name, time);
					oos.writeObject(success);
					oos.flush();
				}
				else if(command.equals("createDir"))
				{
					String dir = (String)ois.readObject();
					String name = (String)ois.readObject();
					
					boolean cDir = createDir(dir, name);
					System.out.println(Thread.currentThread().getName() + " Directory create : "+ cDir);
					oos.writeObject(cDir);
					oos.flush();
				}
				else if(command.equals("uploadFile"))
				{
					String dir = (String)ois.readObject();
					String name = (String)ois.readObject();
					byte[] buffer = (byte[])ois.readObject();
					System.out.println(Thread.currentThread().getName() + " is uploading file <" + name + "> from directory <" + dir + ">");
					boolean succes = uploadFile(dir, name, buffer);
					System.out.println(Thread.currentThread().getName() + " has uploaded file <" + name + "> from directory <" + dir + ">");
					oos.writeObject(succes);
					oos.flush();
				}
				else if(command.equals("renameFile"))
				{
					String dir = (String)ois.readObject();
					String name = (String)ois.readObject();
					String newName = (String)ois.readObject();
					boolean success = renameFile(dir, name, newName);
					oos.writeObject(success);
					oos.flush();
				}
				else if(command.equals("renameDir"))
				{
					String dir = (String)ois.readObject();
					String name = (String)ois.readObject();
					
					boolean rDir = renameDir(dir, name);
					oos.writeObject(rDir);
					oos.flush();
				}
				else if(command.equals("deleteFile"))
				{
					String dir = (String)ois.readObject();
					String name = (String)ois.readObject();
					System.out.println(Thread.currentThread().getName() + " is deleting file <" + name + "> from directory <" + dir + ">");
					boolean succes = deleteFile(dir, name);
					System.out.println(Thread.currentThread().getName() + " has deleted file <" + name + "> from directory <" + dir + ">");
					oos.writeObject(succes);
					oos.flush();
				}
				else if(command.equals("checkDeleteFile"))
				{
					String dir = (String)ois.readObject();
					String name = (String)ois.readObject();
					LocalDateTime time = (LocalDateTime)ois.readObject();

					int successCode = checkDeleteFile(dir, name, time);
					oos.writeObject(successCode);
					oos.flush();
				}
				else if(command.equals("safeDeleteFile"))
				{
					String dir = (String)ois.readObject();
					String name = (String)ois.readObject();
					int mode = (int)ois.readObject();

					DSFileID fileInfo = safeDeleteFile(dir, name, mode);
					oos.writeObject(fileInfo);
					oos.flush();
				}
				else if(command.equals("safeDeleteDir"))
				{
					String dir = (String)ois.readObject();
					System.out.println("safeDeleteDir");
					safeDeleteDir(dir);
					oos.writeObject(true);
					oos.flush();
				}
				else if(command.equals("deleteDir"))
				{
					String dir = (String)ois.readObject();
					String name = (String)ois.readObject();
					
					boolean rDir = deleteDir(dir, name);
					oos.writeObject(rDir);
					oos.flush();
				}
				else if(command.equals("checkDeleteDir"))
				{
					String dir = (String)ois.readObject();
					LocalDateTime t = (LocalDateTime)ois.readObject();
					Integer rDir = checkDeleteDir(dir, t);
					oos.writeObject(rDir);
					oos.flush();
				}
				else if(command.equals("RegisterDS"))
				{
					Address dSAddress = (Address)ois.readObject();
					boolean success = this.registerDS(dSAddress);
					System.out.println(Thread.currentThread().getName() + " Data Server "+ dSAddress.getIp()+":"+dSAddress.getPort()+" added");
					oos.writeObject(success);
					oos.flush();
				}
				else if(command.equals("subscribe"))
				{
					System.out.println("subscribe");
					Address SDirAddress = (Address)ois.readObject();
					boolean success = this.registerServerDir(SDirAddress);
					System.out.println(Thread.currentThread().getName() + " Server Directory"+ SDirAddress.getIp()+":"+SDirAddress.getPort()+" added");
					oos.writeObject(success);
					oos.flush();
				}
				else if (command.equals("checkUploadFile"))
				{
					String dir = (String)ois.readObject();
					String name = (String)ois.readObject();
					LocalDateTime time = (LocalDateTime)ois.readObject();
					
					int check = checkUploadFile(dir, name, time);
					oos.writeObject(check);
					
					if(check == 1)
					{
						oos.writeObject(dirFilesTable.get(dir).get(name));
					}
					oos.flush();
				}
				else if(command.equals("createAssociation"))
				{
					 String dir = (String)ois.readObject();
					 String name = (String)ois.readObject();
					 DSFileID file = (DSFileID)ois.readObject();
					 boolean succes = createAssociation( dir,  name,  file) ;
					 if(succes){
					 	System.out.println(Thread.currentThread().getName() + " has created the association <" + dir + " <-> " + name + ">");
					 }
					 oos.writeObject(succes);
					 oos.flush();
				}
				else if(command.equals("checkCreateDir"))
				{
					String dir = (String)ois.readObject();
					LocalDateTime t = (LocalDateTime)ois.readObject();
					oos.writeObject(checkCreateDir(dir,t));
					oos.flush();
				}
				else if(command.equals("safeRenameDir"))
				{
					String dir = (String)ois.readObject();
					String name = (String)ois.readObject();
					
					safeRenameDir(dir, name);

					oos.writeObject(true);
					oos.flush();
				}
				else if(command.equals("checkRenameDir"))
				{
					String dir = (String)ois.readObject();
					String directory = (String)ois.readObject();
					LocalDateTime time = (LocalDateTime)ois.readObject();
					int cRDir = checkRenameDir(dir,directory,time);
					System.out.println("checkRenameDir ha dato: " + cRDir);
					oos.writeObject(cRDir);
					oos.flush();
				}
				else
				{
					String error = "Bad Request";
					oos.writeObject(error);
					oos.flush();
				}
				oos.close();
				ois.close();
				client.close();
			}
			catch(IOException | ClassNotFoundException ioe)
			{
				//il main thread mi ha "comunicato" che e' tempo di morire...
				return;
			}

			
		}
		
		public boolean registerDS(Address dSAdd) {
			boolean success = false;
			wlds.lock();
			{
				success = serverDataList.add(dSAdd);
			}
			wlds.unlock();
			return success;
		}
		public boolean registerServerDir(Address dSAdd) {
			boolean success = false;
			wlsd.lock();
			{
				success = serverDirList.add(dSAdd);
			}
			wlsd.unlock();
			return success;
		}
		
		@Override
		public boolean createDir(String dir, String name) {
			// TODO Auto-generated method stub
			LocalDateTime time = LocalDateTime.now();
			String directory = new String();
			if(dir.compareTo("/") == 0)
				directory = dir + "" + name;
			else
				directory = dir + "/" + name;
			
			wlDir.lock();
			
				if(wrdDir.get(0).containsKey(directory))
				{
					System.out.println("The directory is in wrdDir");
					return false;
				}
				
				wrdDir.get(0).put(directory, new ConcurrentDirFiles(time));
			
			wlDir.unlock();
			
			int checkTree;
			
			rlTree.lock();

				checkTree = dirTree.contains(directory);
				System.out.println("The directory is in the Tree < "+checkTree+" >");
			rlTree.unlock();
			
			if(checkTree == 2)
			{
				wlDir.lock();
				System.out.println("The directory already exist");
				wrdDir.get(0).remove(directory);
				wlDir.unlock();
				return false;
			}
		
				
			
			wlDir.lock();
			
				Iterator<String> iter = wrdDir.get(2).keySet().iterator();
				
				//controllo che non sia in eliminazione una cartella superiore
				
				while(iter.hasNext())
				{
					String d = iter.next();
					if(directory.startsWith(d) && time.compareTo(wrdDir.get(2).get(d).getTime()) > 0)
					{
						System.out.println("The directory is in wrdDir "+ dir +" - "+d);
						wrdDir.get(0).remove(directory);
						wlDir.unlock();
						return false;
					}
				}
			
			wlDir.unlock();
			
			Iterator<Address> iter2 = serverDirList.iterator();
		
			boolean serverDirCode = false;
			if(dir.compareTo("/") == 0)
				serverDirCode = true;
			
			while(iter2.hasNext())
			{
				Socket server = null;
				
				try
				{
					Address address = iter2.next();
					System.out.println("Check server " + address.getIp()+":"+address.getPort());
					server = new Socket(address.getIp(), address.getPort());
					
					ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
				
					oos.writeObject("checkCreateDir");
					oos.writeObject(directory);
					oos.writeObject(time);
					
					oos.flush();
					
					int dirCheck = (int) ois.readObject();
					
					if(dirCheck == 2) //esiste gia' la directory
					{
						wlDir.lock();
							System.out.println("The directory already exist and was remove from wrdDir");
							wrdDir.get(0).remove(directory);
						
						wlDir.unlock();
						
						return false;
					}
					else if(dirCheck == 1) // esiste la cartella padre
					{
						 System.out.println("The parent directory exist");
						 serverDirCode = true;	
					}
					
					server.close();
				}
				catch(Exception e)
				{
					System.out.println("Error with server: "+ server);
					System.out.println(e);
				}
				
			}	
			
			
			if(serverDirCode)
			{
				
				wlTree.lock();
					System.out.println("The directory added in the Tree");
					dirTree.addDirectory(directory);
				wlTree.unlock();
				
	
				wlDir.lock();
				System.out.println("The directory deleted wrdDir");
				wrdDir.get(0).remove(directory);
				wlDir.unlock();
				
				
				return true;
				
			}
			else
			{

				if(checkTree == 1 || dir.compareTo("/") == 0)
				{
					wlTree.lock();
					dirTree.addDirectory(directory);
					wlTree.unlock();
				}
				else
				{
					wlDir.lock();
					
					wrdDir.get(0).remove(directory);
						
					wlDir.unlock();
					
					return false;
				}
				
			
				wlDir.lock();
				
				wrdDir.get(0).remove(directory);
					
				wlDir.unlock();
				
				return true;
				
			}
			
		
		}
	
		public int checkCreateDir(String dir, LocalDateTime t)
		{
			int dirCode;
			
			rlTree.lock();
				dirCode = dirTree.contains(dir);
			rlTree.unlock();				
				
			if(dirCode == 2)
			{
				System.out.println("The Tree contains the directory");
				return dirCode;
			}


			rlDir.lock();
				
			Iterator<String> iter = wrdDir.get(2).keySet().iterator();
			while(iter.hasNext())
			{
				String d = iter.next();
				if(dir.startsWith(d) && wrdDir.get(2).get(d).getTime().compareTo(t) < 0)
				{
					System.out.println("The directory is in delete");
					rlDir.unlock();
					return 2;

				}
			}
		
			rlDir.unlock();
			
			
			rlDir.lock();
			
			if(wrdDir.get(0).containsKey(dir) && wrdDir.get(0).get(dir).getTime().compareTo(t) < 0)
			{
				System.out.println("The directory is in create");
				dirCode = 2;
			}
			
			rlDir.unlock();
				
			
			return dirCode;							
			
		}
			
		@Override
		public boolean renameDir(String dir, String name) 
		{
			// TODO Auto-generated method stub
			LocalDateTime time = LocalDateTime.now();
			
			String func = new String("checkRenameDir");			
			
			int i = dir.length()-1;
			
			while( dir.charAt(i) != '/')
			{
				i--;
			}
			
			String directory = dir.substring(0, i) + "/" + name;
	
			rlTree.lock();
				if(dirTree.contains(directory) == 2)
				{
					rlTree.unlock();
					return false;
				}
			rlTree.unlock();
			
			wlDir.lock();
			
				Iterator<String> iter = wrdDir.get(2).keySet().iterator();
				while(iter.hasNext())
				{
					String d = iter.next();
					if(dir.startsWith(d) && time.compareTo(wrdDir.get(2).get(d).getTime()) > 0)
					{
						wlDir.unlock();
						return false;
					}
				}
				
				wrdDir.get(2).put(dir, new ConcurrentDirFiles(time));
			
			wlDir.unlock();
			
			wlDir.lock();
			
				iter = wrdDir.get(0).keySet().iterator();
				while(iter.hasNext())
				{
					String d = iter.next();
					if(d.startsWith(directory)  && time.compareTo(wrdDir.get(0).get(d).getTime()) > 0)
					{
						wrdDir.get(2).remove(dir);
						return false;
					}
				}
				wrdDir.get(0).put(directory, new ConcurrentDirFiles(time));

			wlDir.unlock();
			
			
			Iterator<Address> iter2 = serverDirList.iterator();
			ArrayList<Address> serverRenaming = new ArrayList<>();
			System.out.println("il server deve chiedere a seerverDirectory:" + serverDirList.size());
			while(iter2.hasNext())
			{
				Socket server = null;
				
				try
				{
					Address serverPort = iter2.next();
					server = new Socket(serverPort.getIp(), serverPort.getPort());
					
					ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
					System.out.println("server chiede renameDir: " + server + " " + directory);				
					oos.writeObject(func);
					oos.writeObject(dir);
					oos.writeObject(directory);
					oos.writeObject(time);
					
					oos.flush();
					
					int dirCheck = (int) ois.readObject();
					
					
					//server risponde 2 se contiene la cartella da rinominare e si pu?fare
					//		 risponde 1 se non contiene la cartella e si pu?fare
					//		 risponde 0 se non si pu?fare
					if(dirCheck == 2)
					{
					 serverRenaming.add(serverPort);
					}
					else if(dirCheck == 0)
					{
						wlDir.lock();
						
						wrdDir.get(0).remove(directory);
						wrdDir.get(2).remove(dir);

						wlDir.unlock();
						
						return false;
					}
					
					server.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
			}
			
			int checkTree = -1;
				
			rlTree.lock();
				
			if(dirTree.contains(dir) != 2 && serverRenaming.size() == 0)
			{
				checkTree = 0;
			}
			else if(dirTree.contains(dir) == 2)
			{
				checkTree = 1;
			}
			
			rlTree.unlock();
				
				
			if(checkTree == 0)
			{
				wlDir.lock();
				
					wrdDir.get(0).remove(directory);
					wrdDir.get(2).remove(dir);

				wlDir.unlock();
				
				return false;
			}
			else if(checkTree == 1)
			{
				safeRenameDir(dir, name);
			}				
			
			iter2 = serverRenaming.iterator();
			func = new String("safeRenameDir");
			
			while(iter2.hasNext())
			{
				Socket server = null;
				
				try
				{
					Address serverPort = iter2.next();
					server = new Socket(serverPort.getIp(), serverPort.getPort());
					System.out.println("safeRenameDir:" +  server);
					ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
					ObjectInputStream ios = new ObjectInputStream(server.getInputStream());
					
					oos.writeObject(func);
					oos.writeObject(dir);
					oos.writeObject(name);
					
					oos.flush();
					
					boolean renamed = (boolean) ios.readObject();
					
					server.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
			}
			
			wlDir.lock();
			
			wrdDir.get(0).remove(directory);
			wrdDir.get(2).remove(dir);

			wlDir.unlock();
			return true;
		}
			
		public int checkRenameDir(String dir, String directory, LocalDateTime time)
		{
			System.out.println("checkRenameDir chiamato");
			
			rlDir.lock();
			
				Iterator<String> iter = wrdDir.get(2).keySet().iterator();
				while(iter.hasNext())
				{
					String d = iter.next();
					if(dir.startsWith(d) && time.compareTo(wrdDir.get(2).get(d).getTime()) > 0)
					{
						rlDir.unlock();
						return 0;
					}
				}
				
			rlDir.unlock();
			
			rlDir.lock();
			
				iter = wrdDir.get(0).keySet().iterator();
				while(iter.hasNext())
				{
					String d = iter.next();
					if(d.startsWith(directory) && time.compareTo(wrdDir.get(0).get(d).getTime()) > 0)
					{
						rlDir.unlock();
						return 0;
					}
				}
				
			rlDir.unlock();
			
			rlTree.lock();
			
			if(dirTree.contains(directory) == 2)
			{
				rlTree.unlock();
				return 0;
			}

			if(dirTree.contains(dir) == 2)
			{
				rlTree.unlock();
				return 2;
			}
			else
			{
				rlTree.unlock();
				return 1;
			}
		
		}
					
		public synchronized void safeRenameDir(String dir, String name)
		{
			wlTree.lock();
				dirTree.renameDirectory(dir, name);
			wlTree.unlock();
			
			int i = dir.length()-1;
		
			while( dir.charAt(i) != '/')
			{
				i--;
			}
			
			String directory = dir.substring(0, i) + "/" + name;
			
			wl.lock();
			
			HashMap<String,DSFileID> temp = dirFilesTable.remove(dir);
			dirFilesTable.put(directory, temp);
			
			
			Iterator<String> iter = dirTree.findAllchild(dir).iterator();
			while(iter.hasNext())
			{
				
				String s = iter.next();
				String dirRename = directory + s.substring(dir.length(), s.length());
				
				
				temp = dirFilesTable.remove(s);
				dirFilesTable.put(dirRename, temp);
				
			}
			
			wl.unlock();
		}
		
		@Override
		public boolean deleteDir(String dir, String name) 
		{
			// TODO Auto-generated method stub
			int checkCode;
			LocalDateTime time = LocalDateTime.now();
			
			String directory = new String();
			if(dir.compareTo("/") == 0)
				directory = dir + "" + name;
			else
				directory = dir + "/" + name;
			
			rlTree.lock();
				checkCode = dirTree.contains(directory);
				System.out.println("Directory "+ dir+" is in my directoryTree < "+checkCode+" >");
			rlTree.unlock();
			
			wlDir.lock();
			
				Iterator<String> iter = wrdDir.get(2).keySet().iterator();
				while(iter.hasNext())
				{
					String d = iter.next();
					if(dir.startsWith(d) && time.compareTo(wrdDir.get(2).get(d).getTime()) > 0)
					{
						System.out.println("Directory "+ dir+" is in upload");
						wlDir.unlock();
						return false;
					}
				}
				wrdDir.get(2).put(directory, new ConcurrentDirFiles(time));

			wlDir.unlock();
			
			wlDir.lock();
			
				iter = wrdDir.get(0).keySet().iterator();
				while(iter.hasNext())
				{
					String d = iter.next();
					if(d.startsWith(directory)  && time.compareTo(wrdDir.get(0).get(d).getTime()) > 0)
					{
						System.out.println("Directory "+ dir+" is in creation");
						wrdDir.get(2).remove(directory);
						wlDir.unlock();
						return false;
					}
				}
				
			wlDir.unlock();
			
			Iterator<Address> iter2 = serverDirList.iterator();
			ArrayList<Address> serverDeleting = new ArrayList<>();
			
			String func = new String("checkDeleteDir");
			
			while(iter2.hasNext())
			{
				Socket server = null;
				
				try
				{
					Address serverPort = iter2.next();
					server = new Socket(serverPort.getIp(), serverPort.getPort());
					
					ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
				
					oos.writeObject(func);
					oos.writeObject(directory);
					oos.writeObject(time);
					
					oos.flush();
					
					int dirCheck = (int) ois.readObject();
					
					System.out.println("Server "+serverPort+" responses < "+dirCheck+" >");
					
					// risponde 2 se si pu� fare e ha la cartella
					// risponde 1 se si pu� fare
					// risponde 0 se non si pu� fare
					
					if(dirCheck == 2)
					{
					 serverDeleting.add(serverPort);
					}
					else if(dirCheck == 0)
					{
						wlDir.lock();
							wrdDir.get(2).remove(directory);
						wlDir.unlock();
						
						return false;
					}
					
					server.close();
				}
				catch(Exception e)
				{
					System.out.println("Error with server directory "+ server);
					System.out.println(e);
				}
				
			}
			
			iter2 = serverDeleting.iterator();
			func = new String("safeDeleteDir");
			
			while(iter2.hasNext())
			{
				Socket server = null;
				
				try
				{
					Address serverPort = iter2.next();
					server = new Socket(serverPort.getIp(), serverPort.getPort());
					
					ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
					oos.writeObject(func);
					oos.writeObject(directory);
					
					oos.flush();
					
					ois.readObject();
					
					server.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
			}
			
			if(checkCode == 2)
				safeDeleteDir(directory);
			
			wlDir.lock();
				System.out.println("Remove the directory from wrdDir "+directory);
				
				wrdDir.get(2).remove(directory);
			
			
			wlDir.unlock();
			

			return true;
		}
	
		public int checkDeleteDir(String dir, LocalDateTime time)
		{
			System.out.println("The directory is in check delete directory "+dir+" with time "+time);
			// risponde 2 se si pu� fare e ha la cartella
			// risponde 1 se si pu� fare
			// risponde 0 se non si pu� fare
			int checkCode;
			
			rlTree.lock();
			checkCode = dirTree.contains(dir);
			System.out.println("Directory "+ dir+" is in my directoryTree < "+checkCode+" >");
			rlTree.unlock();
			
			if(checkCode == 0)
				checkCode = 1;
			
			rlDir.lock();
		
			Iterator<String> iter = wrdDir.get(2).keySet().iterator();
			while(iter.hasNext())
			{
				String d = iter.next();
				if(dir.startsWith(d) && time.compareTo(wrdDir.get(2).get(d).getTime()) > 0)
				{
					System.out.println("Directory "+ dir+" is in upload");
					rlDir.unlock();
					return 0;
				}
			}

			rlDir.unlock();
		
			rlDir.lock();
		
			iter = wrdDir.get(0).keySet().iterator();
			while(iter.hasNext())
			{
				String d = iter.next();
				if(d.startsWith(dir)  && time.compareTo(wrdDir.get(0).get(d).getTime()) > 0)
				{
					System.out.println("Directory "+ dir+" is in creation");
					rlDir.unlock();
					return 0;
				}
			}
			
		rlDir.unlock();
		return checkCode;
		
		}
		
		public synchronized void safeDeleteDir(String directory)
		{
			System.out.println("Safe Delete Directory "+ directory);
			wlTree.lock();
				dirTree.removeDirectory(directory);
			wlTree.unlock();
			
			Iterator<String> iter = dirTree.findAllchild(directory).iterator();
			HashMap<Address, ArrayList<Integer>> files = new HashMap<>();
			HashMap<String, DSFileID> temp;
			while(iter.hasNext())
			{
				String childDel = iter.next();
				
				wl.lock();
					temp = (HashMap<String,DSFileID>) dirFilesTable.remove(childDel);
				wl.unlock();
				
				Iterator<String> iter2 = temp.keySet().iterator();
				while(iter2.hasNext())
				{
					String s = iter2.next();
					DSFileID t = temp.get(s);
					
					if(!files.containsKey(t.getPos()))
					{
						files.put(t.getPos(), new ArrayList<>());
					}
					
					files.get(t.getPos()).add(t.getKey());
				}
			}
			
			Iterator<Address> iter3 = files.keySet().iterator();
			while(iter3.hasNext())
			{
				Socket server = null;
				
				try
				{
					Address serverPort = iter3.next();
					server = new Socket(serverPort.getIp(), serverPort.getPort());
					
					ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
				
					oos.writeObject("deleteFiles");
					oos.writeObject(files.get(serverPort));

						
					oos.flush();

					
					server.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
			
			}
		}
		
		@Override
		public boolean deleteFile(String dir, String name) {
			// TODO Auto-generated method stub
			
			LocalDateTime time = LocalDateTime.now();
			String filePath = new String(dir+"/"+name);


			
			wlFiles.lock();
			
			if(wudFiles.get(2).containsKey(filePath))
			{
				wlFiles.unlock();
				return false;
			}
			else
			{
				wudFiles.get(2).put(filePath, new ConcurrentDirFiles(time));
				wlFiles.unlock();
			}
		
			
			wlFiles.lock();
			
			if(wudFiles.get(1).containsKey(filePath) && time.compareTo(wrdDir.get(1).get(filePath).getTime()) > 0)
			{
				wudFiles.get(2).remove(filePath);
				wlFiles.unlock();
				return false;
			}
			
			wlFiles.unlock();
		
			wlDir.lock();
		
			Iterator<String> iter = wrdDir.get(2).keySet().iterator();
			while(iter.hasNext())
			{
				String d = iter.next();
				if(dir.startsWith(d) && time.compareTo(wrdDir.get(2).get(d).getTime()) > 0)
				{
					wlFiles.lock();
						wudFiles.get(2).remove(filePath);
					wlFiles.unlock();
					return false;
				}
			}
			wrdDir.get(2).put(dir, new ConcurrentDirFiles(time));

			wlDir.unlock();
		
			
			Iterator<Address> iter2 = serverDirList.iterator();
			Socket server;
			Address serverPort;
			int checkDelDir;
			ArrayList<Address> dirFileDel = new ArrayList<>();
			
			while(iter2.hasNext())
			{
				try
				{
					serverPort = iter2.next();
					server = new Socket(serverPort.getIp(), serverPort.getPort());
					
					ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
				
					oos.writeObject("checkDeleteFile");
					oos.writeObject(dir);
					oos.writeObject(name);
					oos.writeObject(time);

					
					oos.flush();
					
					checkDelDir = (int) ois.readObject();

					if(checkDelDir == 0)
					{
						wlDir.lock();
						if(wrdDir.get(2).get(dir).getCounter() == 1)
							wrdDir.get(2).remove(dir);
						else
							wrdDir.get(2).get(dir).setCounter(wrdDir.get(2).get(dir).getCounter()-1);
						wlDir.unlock();
						
						wlFiles.lock();
						
							wudFiles.get(2).remove(filePath);

						wlFiles.unlock();
						return false;
					}
					else if(checkDelDir == 2 )
					{
						//Address ServerFileDel = (Address) ois.readObject();
						dirFileDel.add(serverPort);
					}
					
					server.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
			}
			boolean checkSafeDel = false;
			int mode = 0;

			wl.lock();
				if(dirFilesTable.containsKey(dir) && dirFilesTable.get(dir).containsKey(name))
				{
					checkSafeDel = true;
					safeDeleteFile(dir, name, mode);
					mode = 1;
				}
				
			wl.unlock();

			if(dirFileDel.size() == 0)
			{
				if(!checkSafeDel)
				{
					wlDir.lock();
					if(wrdDir.get(2).get(dir).getCounter() == 1)
						wrdDir.get(2).remove(dir);
					else
						wrdDir.get(2).get(dir).setCounter(wrdDir.get(2).get(dir).getCounter()-1);
					
					wlDir.unlock();
				
					wlFiles.lock();
				
					wudFiles.get(2).remove(filePath);

					wlFiles.unlock();
					
					return false;
				}
			}
			else{
				iter2 = dirFileDel.iterator();
				while(iter2.hasNext())
				{
					try
					{
						serverPort = iter2.next();
						server = new Socket(serverPort.getIp(), serverPort.getPort());
						
						ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
						ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
						oos.writeObject("safeDeleteFile");
						oos.writeObject(dir);
						oos.writeObject(name);
						oos.writeObject(mode);

						oos.flush();

						mode = 1;
						
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}

			wlDir.lock();
			if(wrdDir.get(2).get(dir).getCounter() == 1)
				wrdDir.get(2).remove(dir);
			else
				wrdDir.get(2).get(dir).setCounter(wrdDir.get(2).get(dir).getCounter()-1);
			
			wlDir.unlock();
		
			wlFiles.lock();
		
			wudFiles.get(2).remove(filePath);

			wlFiles.unlock();
			
			
			return true;
		}

		public int checkDeleteFile(String dir, String name, LocalDateTime time)
		{
		
			int dirCode;
			
			rl.lock();
			
				if(dirFilesTable.containsKey(dir) && dirFilesTable.get(dir).containsKey(name))
					dirCode = 2;
				else
					dirCode = 1;
			
			rl.unlock();
			
			String filePath = dir + "/" + name;
		
			rlFiles.lock();
			
			if(wudFiles.get(2).containsKey(filePath) && wudFiles.get(2).get(filePath).getTime().compareTo(time) > 0)
			{
				rlFiles.unlock();
				return 0;
			}
				
			rlFiles.unlock();
			
			rlDir.lock();
		
			Iterator<String> iter = wrdDir.get(2).keySet().iterator();
			while(iter.hasNext())
			{
				String d = iter.next();
				if(dir.startsWith(d) && time.compareTo(wrdDir.get(2).get(d).getTime()) > 0)
				{
					rlDir.unlock();
					return 0;
				}
			}

			rlDir.unlock();
			

			return dirCode;
		}
				
		public synchronized DSFileID safeDeleteFile(String dir, String name, int mode)
		{
			if(mode == 0){
				Address serverPort;
				int key;
				Address serverPortCopy;
				int keyCopy;
				boolean check;
				Socket s = null;
				
				rl.lock();
					serverPort = dirFilesTable.get(dir).get(name).getPos();
					key = dirFilesTable.get(dir).get(name).getKey();
					serverPortCopy = dirFilesTable.get(dir).get(name).getPosCopy();
					keyCopy = dirFilesTable.get(dir).get(name).getKeyCopy();
				rl.unlock();
				
				try 
				{
					s = new Socket(serverPort.getIp(),serverPort.getPort());
					ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
				
					oos.writeObject("deleteFile");
					oos.writeObject(key);
					check = (boolean) ois.readObject();
					
					oos.flush();
					if(serverPortCopy != null){
						s = new Socket(serverPortCopy.getIp(),serverPortCopy.getPort());
						oos = new ObjectOutputStream(s.getOutputStream());
						ois = new ObjectInputStream(s.getInputStream());
					
						oos.writeObject("deleteFile");
						oos.writeObject(keyCopy);
						check = (boolean) ois.readObject();
						
						oos.flush();
					}
					
					
				}
				catch (NumberFormatException | ClassNotFoundException | IOException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			DSFileID fileInfo = dirFilesTable.get(dir).get(name);
			
			wl.lock();
				dirFilesTable.get(dir).remove(name);
			wl.unlock();
			return fileInfo;
			
		}
		
		@Override
		public boolean uploadFile(String dir, String name, byte[] buffer) {
			// TODO Auto-generated method stub
			LocalDateTime time = LocalDateTime.now();
			String directory = new String();
			if(dir.compareTo("/") == 0)
				directory = dir + "" + name;
			else
				directory = dir + "/" + name;
			boolean check = false;

			wlFiles.lock();
				if(wudFiles.get(1).containsKey(directory))
				{
					wlFiles.unlock();
					return false;
				}
				else
				{
					wudFiles.get(1).put(directory, new ConcurrentDirFiles(time));
					check = true;
				}
				
			wlFiles.unlock();
			
			wlFiles.lock();
				if(wudFiles.get(2).containsKey(directory) && time.compareTo(wudFiles.get(2).get(directory).getTime()) > 0)
				{
					wudFiles.get(2).remove(directory);
					return false;
				}
			wlFiles.unlock();
			
			wlDir.lock();
				if(check)
					wrdDir.get(0).put(dir, new ConcurrentDirFiles(time));
			wlDir.unlock();
			
			check = false;
			
			wlDir.lock();
				Iterator<String> iter = wrdDir.get(2).keySet().iterator();
				while(iter.hasNext())
				{
					String d = iter.next();
					if(dir.startsWith(d) && time.compareTo(wrdDir.get(2).get(d).getTime()) > 0)
					{
						check = true;
						if(wrdDir.get(0).get(dir).getCounter() == 1)
							wrdDir.get(0).remove(dir);
						else
							wrdDir.get(2).get(dir).setCounter(wrdDir.get(2).get(dir).getCounter()-1);
					}
				}
			wlDir.unlock();
			
			wlFiles.lock();
				if(check)
				{
					wudFiles.get(2).remove(directory);
					wlFiles.unlock();
					return false;
				}
				
			wlFiles.unlock();
			
			int checkUploadFile = -1;
			check = false;
			
			DSFileID filePosAndKeys = null;
			
			ArrayList<Address> dataServerPorts = new ArrayList<>();
			ArrayList<Integer> keys = new ArrayList<>();
			
			Iterator<Address> iter2 = serverDirList.iterator();
			
			while(iter2.hasNext())
			{
				Socket server = null;
				
				try
				{	
					
					Address serverPort = iter2.next();
					
					server = new Socket(serverPort.getIp(), serverPort.getPort());
					
					ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
					
					oos.writeObject("checkUploadFile");
					oos.writeObject(dir);
					oos.writeObject(name);
					oos.writeObject(time);
					
					oos.flush();
					
					checkUploadFile = (int) ois.readObject();
					
					if(checkUploadFile == 0)
					{
						//rimuovo file dalle tabelle
						wlFiles.lock();
						
							wudFiles.get(1).remove(directory);

						wlFiles.unlock();
						
						wlDir.lock();
						if(wrdDir.get(0).get(dir).getCounter() == 1)
							wrdDir.get(0).remove(dir);
						else
							wrdDir.get(2).get(dir).setCounter(wrdDir.get(2).get(dir).getCounter()-1);
						wlDir.unlock();
						
						return false;
					}
					else if(checkUploadFile == 1)
					{
						check = true;
						filePosAndKeys = (DSFileID)ois.readObject();
					}
					
					server.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		
			rl.lock();
				
			if((dirFilesTable.containsKey(dir) && dirFilesTable.get(dir).containsKey(name)))
			{
				filePosAndKeys = dirFilesTable.get(dir).get(name);
			}
				
			rl.unlock();
			
			if(filePosAndKeys == null)
			{
				wlFiles.lock();
				
					wudFiles.get(1).remove(directory);
				
				wlFiles.unlock();
				
				wlDir.lock();
				if(wrdDir.get(0).get(dir).getCounter() == 1)
					wrdDir.get(0).remove(dir);
				else
					wrdDir.get(2).get(dir).setCounter(wrdDir.get(2).get(dir).getCounter()-1);
				wlDir.unlock();
				
				return false;
			
			}
			else{
				Socket dataServer = null;
				dataServerPorts.add(filePosAndKeys.getPos());
				keys.add(filePosAndKeys.getKey());
				Address SDCopy = filePosAndKeys.getPosCopy();
				if(SDCopy != null){
					dataServerPorts.add(filePosAndKeys.getPosCopy());
					keys.add(filePosAndKeys.getKeyCopy());
				}
				for(int i=0;i<keys.size();i++)
				{

					try 
					{
						dataServer = new Socket(dataServerPorts.get(i).getIp(), dataServerPorts.get(i).getPort());
						
						ObjectOutputStream oos = new ObjectOutputStream(dataServer.getOutputStream());
						ObjectInputStream ois = new ObjectInputStream(dataServer.getInputStream());
						
						oos.writeObject("uploadFile");
						oos.writeObject(keys.get(i));
						oos.writeObject(buffer);
						
						oos.flush();
						boolean ok = (boolean) ois.readObject();
						
					} 
					catch (NumberFormatException | ClassNotFoundException | IOException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
				wlFiles.lock();
				
					wudFiles.get(1).remove(directory);
				
				wlFiles.unlock();
				
				wlDir.lock();
				if(wrdDir.get(0).get(dir).getCounter() == 1)	
					wrdDir.get(0).remove(dir);
				else
					wrdDir.get(2).get(dir).setCounter(wrdDir.get(2).get(dir).getCounter()-1);
					
				wlDir.unlock();
			
			return true;
		}
	
		public int checkUploadFile(String dir, String name, LocalDateTime time)
		{
			String directory = new String();
			if(dir.compareTo("/") == 0)
				directory = dir + "" + name;
			else
				directory = dir + "/" + name;
			
			rlFiles.lock();
			
			if(wudFiles.get(1).containsKey(directory) && time.compareTo(wudFiles.get(2).get(directory).getTime()) > 0)
			{
				rlFiles.unlock();
				return 0;
			}
			
			rlFiles.unlock();
			
			rlFiles.lock();
			
			if(wudFiles.get(2).containsKey(directory) && time.compareTo(wudFiles.get(2).get(directory).getTime()) > 0)
			{
				return 0;
			}
				
			rlFiles.unlock();
			
			rlDir.lock();
			
				Iterator<String> iter = wrdDir.get(2).keySet().iterator();
				while(iter.hasNext())
				{
					String d = iter.next();
					if(dir.startsWith(d) && time.compareTo(wrdDir.get(2).get(d).getTime()) > 0)
					{
						return 0;
					}
				}
				
			rlDir.unlock();
			
			rl.lock();
				if(dirFilesTable.containsKey(dir) && dirFilesTable.get(dir).containsKey(name))
				{
					rl.unlock();
					return 1;
				}
				
			rl.unlock();
			
			
			return 2;
		}
	
		@Override
		public Set<String> lookDirFiles(String dir) {
			HashSet<String> content = new HashSet<String>();
			content = (HashSet)checkMyDir(dir);
			
			Iterator<Address> dirIter = serverDirList.iterator();
			Address server = null;
			while(dirIter.hasNext()) 
			{
				try{
					server = dirIter.next();
					Socket dirs = new Socket(server.getIp(), server.getPort());
					HashSet<String> otherDir = new HashSet<String>();
					ObjectOutputStream oos = new ObjectOutputStream(dirs.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(dirs.getInputStream());
					String func = "checkMyDir";
					oos.writeObject(func);
					oos.writeObject(dir);
					oos.flush();
					otherDir = (HashSet<String>)ois.readObject();
					dirs.close();
					if(!otherDir.isEmpty())
						content.addAll(otherDir);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			return content;
		}
		
		public Set<String> checkMyDir( String dir){
			HashSet<String> content = new HashSet<String>();
			if(dirTree.contains(dir) == 2)
			{
				ArrayList<String> directories = (ArrayList<String>)dirTree.getDirectory(dir);
				for(int i = 0; i < directories.size(); i++)
				{
					content.add(directories.get(i));
				}
			}
			else
				return content;

			if(dirFilesTable.containsKey(dir))
			{	
				Map<String, DSFileID> files = dirFilesTable.get(dir);
				if(files == null)
				{
					return content;
				}
				
				for(String key : files.keySet())
				{
						content.add(key);
				}
			}
			return content;	
		}
	
		@Override
		public boolean fileIsHere(String dir, String name) {
			Map<String, DSFileID> files = dirFilesTable.get(dir);
			DSFileID f = files.get(name);
			if(f == null)
				return false;
			return true;
		}
	
		@Override
		public int isCreatableFile(String dir, String name) {
			// TODO Auto-generated method stub
			return 0;
		}
	
		@Override
		public boolean createFile(String dir, String name, byte[] buffer) {
		
			LocalDateTime time = LocalDateTime.now();
			String directory = new String();
			if(dir.compareTo("/") == 0)
				directory = dir + "" + name;
			else
				directory = dir + "/" + name;
			boolean check = false;

			
			rl.lock();
			
			
			if((dirFilesTable.containsKey(dir) && dirFilesTable.get(dir).containsKey(name)))
			{
				return false;
			}
				
			rl.unlock();
			
			wlFiles.lock();
			
			if(wudFiles.get(0).containsKey(directory) && time.compareTo(wudFiles.get(0).get(directory).getTime()) > 0)
			{
				return false;
			}
			wudFiles.get(0).put(directory, new ConcurrentDirFiles(time));
			
			wlFiles.unlock();
			
			wlFiles.lock();
			
			if(wudFiles.get(2).containsKey(directory) && time.compareTo(wudFiles.get(2).get(directory).getTime()) > 0)
			{
				wudFiles.get(0).remove(directory);
				wlFiles.unlock();
				return false;
			}
				
			wlFiles.unlock();

			wlDir.lock();
			
			wrdDir.get(0).put(dir, new ConcurrentDirFiles(time));
			
			wlDir.unlock();
			
			wlDir.lock();
				Iterator<String> iter = wrdDir.get(2).keySet().iterator();
				while(iter.hasNext())
				{
					String d = iter.next();
					if(dir.startsWith(d) && time.compareTo(wrdDir.get(2).get(d).getTime()) > 0)
					{
						check = true;
						if(wrdDir.get(0).get(dir).getCounter() == 1)
							wrdDir.get(0).remove(dir);
						else
							wrdDir.get(2).get(dir).setCounter(wrdDir.get(2).get(dir).getCounter()-1);
					}
				}
			wlDir.unlock();
			
			wlFiles.lock();
				if(check)
				{
					wudFiles.get(0).remove(directory);
					wlFiles.unlock();
					return false;
				}
				
			wlFiles.unlock();
			
			int checkCreateFile = -1;
			check = false;
			boolean checkDir = false;
			Address dataServerPort = null;
			int key = -1;
			
			Iterator<Address> iter2 = serverDirList.iterator();
			
			while(iter2.hasNext())
			{
				Socket server = null;
				
				try
				{	
					
					Address serverPort = iter2.next();
					
					server = new Socket(serverPort.getIp(), serverPort.getPort());
					
					ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
					
					oos.writeObject("checkCreateFile");
					oos.writeObject(dir);
					oos.writeObject(name);
					oos.writeObject(time);
					
					oos.flush();
					
					checkCreateFile = (int) ois.readObject();
					
					if(checkCreateFile == 0)
					{
						//rimuovo file dalle tabelle
						wlFiles.lock();
						
							wudFiles.get(0).remove(directory);

						wlFiles.unlock();
						
						wlDir.lock();
						if(wrdDir.get(0).get(dir).getCounter() == 1)
							wrdDir.get(0).remove(dir);
						else
							wrdDir.get(2).get(dir).setCounter(wrdDir.get(2).get(dir).getCounter()-1);
						wlDir.unlock();
						
						return false;
					}
					else if(checkCreateFile == 1)
					{
						check = true;

					}
					
					server.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		
			rlTree.lock();
			
			if(dirTree.contains(dir) ==2)
				checkDir = true;
			System.out.println(check+ "  " + checkDir);
			rlTree.unlock();
			
			if(check || checkDir)
			{
		

				iter2 = serverDataList.iterator();
				dataServerPort = iter2.next();
				Socket dataServer = null;
				
				try 
				{
					dataServer = new Socket(dataServerPort.getIp(), dataServerPort.getPort());
					
					ObjectOutputStream oos = new ObjectOutputStream(dataServer.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(dataServer.getInputStream());
					
					oos.writeObject("createFile");
					oos.writeObject(dir);
					oos.writeObject(name);
					oos.writeObject(buffer);
					if(serverDataList.size() < 2){
						Address secondDS = null;
						oos.writeObject(secondDS);	
					}
					else
						oos.writeObject(iter2.next());
					
					boolean ok = (boolean) ois.readObject();
					
					oos.flush();
				} 
				catch (NumberFormatException | ClassNotFoundException | IOException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			
			wlFiles.lock();
			
				wudFiles.get(0).remove(directory);
			
			wlFiles.unlock();
			
			wlDir.lock();
			if(wrdDir.get(0).get(dir).getCounter() == 1)
				wrdDir.get(0).remove(dir);
			else
				wrdDir.get(2).get(dir).setCounter(wrdDir.get(2).get(dir).getCounter()-1);
			wlDir.unlock();
		
			if(check || checkDir)
				return true;
			else
				return false;
		
	

		}
	
		public DSFileID getFilePos(String dir, String name) {
			DSFileID dataS = getMyFilePos(dir, name);
			Iterator<Address> dirIter = serverDirList.iterator(); 
			while(dataS.getPos() == null && dirIter.hasNext())
			{
				try{
					Address server = dirIter.next();
					Socket dirs = new Socket(server.getIp(), server.getPort());
					ObjectOutputStream oos = new ObjectOutputStream(dirs.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(dirs.getInputStream());
					String func = "getMyFilePos";
					oos.writeObject(func);
					oos.writeObject(dir);
					oos.writeObject(name);
					oos.flush();
					
					dataS = (DSFileID)ois.readObject();
					dirs.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}

			}
			return dataS;
		}
				
		public DSFileID getMyFilePos(String dir, String name) {
			
			rl.lock();
			
				if(dirFilesTable.containsKey(dir) && dirFilesTable.get(dir).containsKey(name))
				{
					DSFileID f = dirFilesTable.get(dir).get(name);
					rl.unlock();
					return f;
				}
				
			rl.unlock();
			
			return null;
		}
		
		public int checkCreateFile(String dir, String name, LocalDateTime time)
		{
			String directory = new String();
			if(dir.compareTo("/") == 0)
				directory = dir + "" + name;
			else
				directory = dir + "/" + name;
			
			rlFiles.lock();
			
			if(wudFiles.get(1).containsKey(directory) && time.compareTo(wudFiles.get(2).get(directory).getTime()) > 0)
			{
				rlFiles.unlock();
				return 0;
			}
			
			rlFiles.unlock();
			
			rlFiles.lock();
			
			if(wudFiles.get(2).containsKey(directory) && time.compareTo(wudFiles.get(2).get(directory).getTime()) > 0)
			{
				return 0;
			}
				
			rlFiles.unlock();
			
			rlDir.lock();
			
				Iterator<String> iter = wrdDir.get(2).keySet().iterator();
				while(iter.hasNext())
				{
					String d = iter.next();
					if(dir.startsWith(d) && time.compareTo(wrdDir.get(2).get(d).getTime()) > 0)
					{
						return 0;
					}
				}
				
			rlDir.unlock();
			
			rl.lock();
				if(dirFilesTable.containsKey(dir))
				{
					if(dirFilesTable.get(dir).containsKey(name))
					{
						rl.unlock();
						return 0;
					}
					else
					{
						rl.unlock();
						return 1;
					}
				}
				
			rl.unlock();
			
			return 2;
		}
		
		@Override
		public byte[] getFile(String dir, String name) {
			DSFileID dataS = getFilePos(dir, name);
			byte[] buff = null;
			if(dataS.getPos() == null )
				return buff;
			try{
				Socket dirs = new Socket(dataS.getPos().getIp(), dataS.getPos().getPort());
				ObjectOutputStream oos = new ObjectOutputStream(dirs.getOutputStream());
				ObjectInputStream ois = new ObjectInputStream(dirs.getInputStream());
				
				String func = "readFile";
				oos.writeObject(func);
				oos.writeObject(dataS.getKey());
				oos.flush();
				buff = (byte[])ois.readObject();
				if(buff == null)
					System.out.println("buff null");
				dirs.close();
				if(buff == null && dataS.getPosCopy() != null )
				{
					dirs = new Socket(dataS.getPosCopy().getIp(), dataS.getPosCopy().getPort());
					oos = new ObjectOutputStream(dirs.getOutputStream());
					ois = new ObjectInputStream(dirs.getInputStream());
					oos.writeObject(func);
					oos.writeObject(dataS.getKeyCopy());
					oos.flush();
					buff = (byte[])ois.readObject();
					dirs.close();
				}
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return buff;
			}
			return buff;
		}
	
		@Override
		public boolean createAssociation(String dir, String name, DSFileID file) 
		{
			// TODO Auto-generated method stub
			wl.lock();

				if(dirFilesTable.containsKey(dir))
				{
					
					dirFilesTable.get(dir).put(name, file);
					
				}
				else 
				{
					HashMap<String, DSFileID>  tmp= new HashMap<>();
					tmp.put(name, file);
					dirFilesTable.put(dir, tmp);
					
				}
			wl.unlock();
			
			wlTree.lock();

				if(dirTree.contains(dir) != 2)
					dirTree.addDirectory(dir);
			wlTree.unlock();
			
			return true;
		}

		public boolean renameFile(String dir, String name, String newName){
			
			LocalDateTime time = LocalDateTime.now();
			String filePath = new String();
			String newFilePath = new String();
			if(dir.compareTo("/") == 0){
				filePath = dir +  name;
				newFilePath = dir + newName;
			}
			else{
				filePath = dir + "/" + name;
				newFilePath = dir + "/" + newName;
			}
			
			rl.lock();
			
			if((dirFilesTable.containsKey(dir) && dirFilesTable.get(dir).containsKey(newName)))//controllo che il nuovo nome non esista già
			{
				return false;
			}
				
			rl.unlock();
			
			wlFiles.lock();
			
			if(wudFiles.get(2).containsKey(filePath) || wudFiles.get(0).containsKey(newFilePath) || wudFiles.get(1).containsKey(filePath))//controllo che il vecchio non sia in eliminazione/upload o il nuovo in creazione
			{
				wlFiles.unlock();
				return false;
			}
			else
			{
				wudFiles.get(2).put(filePath, new ConcurrentDirFiles(time));
				wudFiles.get(1).put(filePath, new ConcurrentDirFiles(time));
				wudFiles.get(0).put(newFilePath, new ConcurrentDirFiles(time));
			}
		
			wlFiles.unlock();	

			wlDir.lock();
		
			Iterator<String> iter = wrdDir.get(2).keySet().iterator();
			while(iter.hasNext())
			{
				String d = iter.next();
				if(dir.startsWith(d))
				{
					wlFiles.lock();
						wudFiles.get(2).remove(filePath);
						wudFiles.get(1).remove(filePath);
						wudFiles.get(0).remove(newFilePath);
					wlFiles.unlock();
					return false;
				}
			}
			wrdDir.get(2).put(dir, new ConcurrentDirFiles(time));

			wlDir.unlock();

			Iterator<Address> iter2 = serverDirList.iterator();
			Socket server;
			Address serverPort;
			int checkDelDir, checkCreateFile;
			ArrayList<Address> dirFileDel = new ArrayList<>();
			
			while(iter2.hasNext())
			{
				try
				{
					serverPort = iter2.next();
					server = new Socket(serverPort.getIp(), serverPort.getPort());
					
					ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
				
					oos.writeObject("checkDeleteFile");
					oos.writeObject(dir);
					oos.writeObject(name);
					oos.writeObject(time);
					
					oos.flush();

					checkDelDir = (int) ois.readObject();

					server = new Socket(serverPort.getIp(), serverPort.getPort());
					oos = new ObjectOutputStream(server.getOutputStream());
					ois = new ObjectInputStream(server.getInputStream());
				
					oos.writeObject("checkCreateFile");
					oos.writeObject(dir);
					oos.writeObject(newName);
					oos.writeObject(time);
					
					oos.flush();
					
					checkCreateFile = (int) ois.readObject();
					
					if(checkDelDir == 0 || checkCreateFile == 0)
					{
						wlDir.lock();
						if(wrdDir.get(2).get(dir).getCounter() == 1)
							wrdDir.get(2).remove(dir);
						else
							wrdDir.get(2).get(dir).setCounter(wrdDir.get(2).get(dir).getCounter()-1);
						wlDir.unlock();
						
						wlFiles.lock();
						
							wudFiles.get(2).remove(filePath);
							wudFiles.get(1).remove(filePath);
							wudFiles.get(0).remove(newFilePath);

						wlFiles.unlock();
						return false;
					}
					else if(checkDelDir == 2)
					{
						//Address ServerFileDel = (Address) ois.readObject();
						dirFileDel.add(serverPort);
					}
					
					server.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
			}

			int mode = 1;
			DSFileID fileInfo = null;
			boolean checkSafeDel = false;
			wl.lock();
				if(dirFilesTable.containsKey(dir) && dirFilesTable.get(dir).containsKey(name))
				{
					fileInfo = dirFilesTable.get(dir).get(name);
					checkSafeDel = true;
					safeDeleteFile(dir, name, mode);
					createAssociation(dir, newName, fileInfo);
				}
				
			wl.unlock();
			if(dirFileDel.size() == 0)
			{
				if(!checkSafeDel)
				{
					wlDir.lock();
					if(wrdDir.get(2).get(dir).getCounter() == 1)
						wrdDir.get(2).remove(dir);
					else
						wrdDir.get(2).get(dir).setCounter(wrdDir.get(2).get(dir).getCounter()-1);
					
					wlDir.unlock();
				
					wlFiles.lock();
				
					wudFiles.get(2).remove(filePath);
					wudFiles.get(1).remove(filePath);
					wudFiles.get(0).remove(newFilePath);

					wlFiles.unlock();
					
					return false;
				}
			}
			else{
				iter2 = dirFileDel.iterator();
				while(iter2.hasNext())
				{
					try
					{
						serverPort = iter2.next();
						server = new Socket(serverPort.getIp(), serverPort.getPort());
						
						ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
						ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
						oos.writeObject("safeDeleteFile");
						oos.writeObject(dir);
						oos.writeObject(name);
						oos.writeObject(mode);

						oos.flush();
						fileInfo = (DSFileID)ois.readObject();
						System.out.println(fileInfo);

						server = new Socket(serverPort.getIp(), serverPort.getPort());
						oos = new ObjectOutputStream(server.getOutputStream());
						ois = new ObjectInputStream(server.getInputStream());

						oos.writeObject("createAssociation");
						oos.writeObject(dir);
						oos.writeObject(newName);
						oos.writeObject(fileInfo);
						oos.flush();
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
			wlDir.lock();
			if(wrdDir.get(2).get(dir).getCounter() == 1)
				wrdDir.get(2).remove(dir);
			else
				wrdDir.get(2).get(dir).setCounter(wrdDir.get(2).get(dir).getCounter()-1);
			
			wlDir.unlock();
		
			wlFiles.lock();
		
			wudFiles.get(2).remove(filePath);
			wudFiles.get(1).remove(filePath);
			wudFiles.get(0).remove(newFilePath);

			wlFiles.unlock();
			
			
			return true;

		}
	
		@Override
		public boolean activation() {
			// TODO Auto-generated method stub
			
			Socket server = null;
			
			try
			{
				server = new Socket("localhost", 1234);
				
				ObjectOutputStream oos = new ObjectOutputStream(server.getOutputStream());
				ObjectInputStream ois = new ObjectInputStream(server.getInputStream());
			
				oos.writeObject("addServerDir");
				oos.writeObject("2233");
				oos.flush();
				
				HashSet<Address> temp = (HashSet<Address>) ois.readObject();
				
				serverDirList.addAll(temp);
				
				
				HashSet<Address> t = (HashSet<Address>) ois.readObject();
				
				serverDataList.addAll(t);
				
				oos.writeObject(true);
				
				server.close();
				return true;
			}
			catch(Exception e)
			{
				try {
					oos.writeObject(false);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
			
		
			return false;
		}
	
		@Override
		public boolean status() {
			// TODO Auto-generated method stub
			return true;
		}
		
			
		
		
		
	}
}